//Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
//--------------------------------------------------------------------------------
//Tool Version: Vivado v.2020.2 (lin64) Build 3064766 Wed Nov 18 09:12:47 MST 2020
//Date        : Thu Aug 19 18:58:31 2021
//Host        : 10.0.0.102 running 64-bit openSUSE Tumbleweed
//Command     : generate_target design_1_wrapper.bd
//Design      : design_1_wrapper
//Purpose     : IP block netlist
//--------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

module design_1_wrapper
   (DDR_addr,
    DDR_ba,
    DDR_cas_n,
    DDR_ck_n,
    DDR_ck_p,
    DDR_cke,
    DDR_cs_n,
    DDR_dm,
    DDR_dq,
    DDR_dqs_n,
    DDR_dqs_p,
    DDR_odt,
    DDR_ras_n,
    DDR_reset_n,
    DDR_we_n,
    FIXED_IO_ddr_vrn,
    FIXED_IO_ddr_vrp,
    FIXED_IO_mio,
    FIXED_IO_ps_clk,
    FIXED_IO_ps_porb,
    FIXED_IO_ps_srstb,
    MISO_0,
    MOSI_0,
    SS_0,
    board_clock,
    dip_sw0,
    dip_sw1,
    iq_rx_clk_n,
    iq_rx_clk_p,
    iq_txd_n,
    iq_txd_p,
    leds_8bits_tri_o,
    rxd_n,
    rxd_p,
    sck_i_0,
    txclk_n,
    txclk_p);
  inout [14:0]DDR_addr;
  inout [2:0]DDR_ba;
  inout DDR_cas_n;
  inout DDR_ck_n;
  inout DDR_ck_p;
  inout DDR_cke;
  inout DDR_cs_n;
  inout [3:0]DDR_dm;
  inout [31:0]DDR_dq;
  inout [3:0]DDR_dqs_n;
  inout [3:0]DDR_dqs_p;
  inout DDR_odt;
  inout DDR_ras_n;
  inout DDR_reset_n;
  inout DDR_we_n;
  inout FIXED_IO_ddr_vrn;
  inout FIXED_IO_ddr_vrp;
  inout [53:0]FIXED_IO_mio;
  inout FIXED_IO_ps_clk;
  inout FIXED_IO_ps_porb;
  inout FIXED_IO_ps_srstb;
  output MISO_0;
  input MOSI_0;
  input SS_0;
  input board_clock;
  input dip_sw0;
  input dip_sw1;
  input iq_rx_clk_n;
  input iq_rx_clk_p;
  output [0:0]iq_txd_n;
  output [0:0]iq_txd_p;
  output [7:0]leds_8bits_tri_o;
  input [1:0]rxd_n;
  input [1:0]rxd_p;
  input sck_i_0;
  output [0:0]txclk_n;
  output [0:0]txclk_p;

  wire [14:0]DDR_addr;
  wire [2:0]DDR_ba;
  wire DDR_cas_n;
  wire DDR_ck_n;
  wire DDR_ck_p;
  wire DDR_cke;
  wire DDR_cs_n;
  wire [3:0]DDR_dm;
  wire [31:0]DDR_dq;
  wire [3:0]DDR_dqs_n;
  wire [3:0]DDR_dqs_p;
  wire DDR_odt;
  wire DDR_ras_n;
  wire DDR_reset_n;
  wire DDR_we_n;
  wire FIXED_IO_ddr_vrn;
  wire FIXED_IO_ddr_vrp;
  wire [53:0]FIXED_IO_mio;
  wire FIXED_IO_ps_clk;
  wire FIXED_IO_ps_porb;
  wire FIXED_IO_ps_srstb;
  wire MISO_0;
  wire MOSI_0;
  wire SS_0;
  wire board_clock;
  wire dip_sw0;
  wire dip_sw1;
  wire iq_rx_clk_n;
  wire iq_rx_clk_p;
  wire [0:0]iq_txd_n;
  wire [0:0]iq_txd_p;
  wire [7:0]leds_8bits_tri_o;
  wire [1:0]rxd_n;
  wire [1:0]rxd_p;
  wire sck_i_0;
  wire [0:0]txclk_n;
  wire [0:0]txclk_p;

  design_1 design_1_i
       (.DDR_addr(DDR_addr),
        .DDR_ba(DDR_ba),
        .DDR_cas_n(DDR_cas_n),
        .DDR_ck_n(DDR_ck_n),
        .DDR_ck_p(DDR_ck_p),
        .DDR_cke(DDR_cke),
        .DDR_cs_n(DDR_cs_n),
        .DDR_dm(DDR_dm),
        .DDR_dq(DDR_dq),
        .DDR_dqs_n(DDR_dqs_n),
        .DDR_dqs_p(DDR_dqs_p),
        .DDR_odt(DDR_odt),
        .DDR_ras_n(DDR_ras_n),
        .DDR_reset_n(DDR_reset_n),
        .DDR_we_n(DDR_we_n),
        .FIXED_IO_ddr_vrn(FIXED_IO_ddr_vrn),
        .FIXED_IO_ddr_vrp(FIXED_IO_ddr_vrp),
        .FIXED_IO_mio(FIXED_IO_mio),
        .FIXED_IO_ps_clk(FIXED_IO_ps_clk),
        .FIXED_IO_ps_porb(FIXED_IO_ps_porb),
        .FIXED_IO_ps_srstb(FIXED_IO_ps_srstb),
        .MISO_0(MISO_0),
        .MOSI_0(MOSI_0),
        .SS_0(SS_0),
        .board_clock(board_clock),
        .dip_sw0(dip_sw0),
        .dip_sw1(dip_sw1),
        .iq_rx_clk_n(iq_rx_clk_n),
        .iq_rx_clk_p(iq_rx_clk_p),
        .iq_txd_n(iq_txd_n),
        .iq_txd_p(iq_txd_p),
        .leds_8bits_tri_o(leds_8bits_tri_o),
        .rxd_n(rxd_n),
        .rxd_p(rxd_p),
        .sck_i_0(sck_i_0),
        .txclk_n(txclk_n),
        .txclk_p(txclk_p));
endmodule
