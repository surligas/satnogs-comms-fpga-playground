create_clock -period 15.625 -name iq_rx_clk_p -waveform {0.000 7.813} [get_ports iq_rx_clk_p]

set_property IOSTANDARD LVCMOS33 [get_ports SS_0]
set_property PACKAGE_PIN AA9 [get_ports sck_i_0]
set_property IOSTANDARD LVCMOS33 [get_ports sck_i_0]
set_property PACKAGE_PIN AA11 [get_ports MOSI_0]
set_property IOSTANDARD LVCMOS33 [get_ports MOSI_0]
set_property PACKAGE_PIN Y10 [get_ports MISO_0]
set_property IOSTANDARD LVCMOS33 [get_ports MISO_0]

set_property PACKAGE_PIN Y9 [get_ports board_clock]
set_property IOSTANDARD LVCMOS33 [get_ports board_clock]

# LVDS IQ related pins
set_property PACKAGE_PIN M19 [get_ports iq_rx_clk_p]
set_property PACKAGE_PIN R19 [get_ports {rxd_p[0]}]
set_property PACKAGE_PIN P17 [get_ports {rxd_p[1]}]
set_property PACKAGE_PIN N19 [get_ports {txclk_p[0]}]
set_property PACKAGE_PIN P20 [get_ports {iq_txd_p[0]}]


set_property IOSTANDARD LVDS_25 [get_ports iq_rx_clk_p]
set_property IOSTANDARD LVDS_25 [get_ports iq_rx_clk_n]
set_property IOSTANDARD LVDS_25 [get_ports {rxd_p[0]}]
set_property IOSTANDARD LVDS_25 [get_ports {rxd_p[1]}]
set_property IOSTANDARD LVDS_25 [get_ports {rxd_n[0]}]
set_property IOSTANDARD LVDS_25 [get_ports {rxd_n[1]}]
set_property IOSTANDARD LVDS_25 [get_ports {txclk_p[0]}]
set_property IOSTANDARD LVDS_25 [get_ports {txclk_n[0]}]
set_property IOSTANDARD LVDS_25 [get_ports {iq_txd_p[0]}]
set_property IOSTANDARD LVDS_25 [get_ports {iq_txd_n[0]}]

set_property DIFF_TERM TRUE [get_ports iq_rx_clk_p]
set_property DIFF_TERM TRUE [get_ports iq_rx_clk_n]
set_property DIFF_TERM TRUE [get_ports {rxd_p[0]}]
set_property DIFF_TERM TRUE [get_ports {rxd_p[1]}]
set_property DIFF_TERM TRUE [get_ports {rxd_n[0]}]
set_property DIFF_TERM TRUE [get_ports {rxd_n[1]}]
set_property DIFF_TERM TRUE [get_ports {txclk_p[0]}]
set_property DIFF_TERM TRUE [get_ports {txclk_n[0]}]
set_property DIFF_TERM TRUE [get_ports {iq_txd_p[0]}]
set_property DIFF_TERM TRUE [get_ports {iq_txd_n[0]}]

set_property PACKAGE_PIN F22 [get_ports dip_sw0]
set_property IOSTANDARD LVCMOS25 [get_ports dip_sw0]


set_property PACKAGE_PIN Y11 [get_ports SS_0]



set_property PACKAGE_PIN G22 [get_ports dip_sw1]

set_property IOSTANDARD LVCMOS25 [get_ports dip_sw1]





set_property C_CLK_INPUT_FREQ_HZ 300000000 [get_debug_cores dbg_hub]
set_property C_ENABLE_CLK_DIVIDER false [get_debug_cores dbg_hub]
set_property C_USER_SCAN_CHAIN 1 [get_debug_cores dbg_hub]
connect_debug_port dbg_hub/clk [get_nets clk]
