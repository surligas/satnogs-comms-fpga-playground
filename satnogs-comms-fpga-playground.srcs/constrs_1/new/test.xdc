set_property IOSTANDARD DIFF_HSUL_12 [get_ports iq_rx_clk_p]
set_property PACKAGE_PIN V5 [get_ports {iq_tx_clk_p[0]}]
set_property IOSTANDARD DIFF_HSUL_12 [get_ports {iq_tx_clk_p[0]}]
set_property PACKAGE_PIN P17 [get_ports {iq_txd_p[0]}]
set_property IOSTANDARD DIFF_HSUL_12 [get_ports {iq_txd_p[0]}]
set_property PACKAGE_PIN R19 [get_ports iq_rxd24_p]
set_property IOSTANDARD DIFF_HSUL_12 [get_ports iq_rxd24_p]
set_property IOSTANDARD DIFF_HSUL_12 [get_ports iq_rxd09_n]


set_property PACKAGE_PIN M19 [get_ports iq_rx_clk_p]

set_property PACKAGE_PIN Y16 [get_ports SS_0]
set_property PACKAGE_PIN U15 [get_ports sck_i_0]
set_property PACKAGE_PIN U16 [get_ports MOSI_0]
set_property PACKAGE_PIN U17 [get_ports MISO_0]

set_property DRIVE 12 [get_ports MISO_0]

set_property IOSTANDARD LVCMOS33 [get_ports SS_0]
set_property IOSTANDARD LVCMOS33 [get_ports sck_i_0]
set_property IOSTANDARD LVCMOS33 [get_ports MOSI_0]
set_property IOSTANDARD LVCMOS33 [get_ports MISO_0]
